<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator" prefix="decorator" %>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<decorator:head />
	
	<!-- stylesheets -->
	<link rel="stylesheet" href="${pageContext.request.contextPath}/css/me.css" type="text/css" />
	<link rel="stylesheet" href="${pageContext.request.contextPath}/css/foundation.min.css" />
	
	<!-- preloaded scripts -->
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/vendor/modernizr.js"></script>
</head>
<body>
	<header class="row">
	    <h1 class="small-text-center"><decorator:title /></h1>
	</header>
    <hr />
    <decorator:body />
    <hr />
    <footer class="row">
	    <p class="small-text-center">&copy; 2015 Expeditors Training</p>
    </footer>
</body>
</html>