<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Add Product</title>
</head>
<body>
<form method="POST" action="addProduct">
	<div class="row">
		<p class="small-12 columns small-text-center">
			Name: <input id="name" type="text" name="name" /><br />
		</p>
	</div>
	<div class="row">
		<p class="small-12 columns small-text-center">
			Description: <input id="description" type="text" name="description" /><br />
		</p>
	</div>
	<div class="row">
		<p class="small-12 columns small-text-center">
			Price: <input id="price" type="text" name="price" /><br />
		</p>
	</div>
	<div class="row">
		<div class="small-12 small-text-center columns">
			<input class="button" id="submit" type="submit" value="Submit" />
		</div>
	</div>

</form>
</body>
</html>