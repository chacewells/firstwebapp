<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<title>Contact Me</title>
	
	<!-- stylesheets -->
	<link rel="stylesheet" href="css/me.css" type="text/css" />
	<link rel="stylesheet" href="${pageContext.request.contextPath}/css/foundation.min.css" />
	
	<!-- preloaded scripts -->
	<script type="text/javascript" src="js/vendor/modernizr.js"></script>
</head>
<body>
	<form method="POST" action="submitContact" class="row">
		<div class="row">
			<div class="small-1 medium-0 columns"></div>
			<div
				class="small-11 medium-4 columns small-only-text-left text-right">
				email:</div>
			<div class="medium-7 medium-pull-1 columns">
				<input id="email" type="text" name="email" value="${param.email}" />
			</div>
		</div>
		<div class="row">
			<div class="medium-4 columns small-only-text-left text-right">message:</div>
			<div class="medium-7 medium-pull-1 columns">
				<textarea rows="5" name="message" id="message">${param.message}</textarea>
			</div>
		</div>
		<div class="row">
			<div class="medium-4 columns small-only-text-center text-right">
				<input id="submit" type="submit" value="Submit"
					class="button radius" />
			</div>
			<div class="medium-8 columns"></div>
		</div>
	</form>
	
	<script src="js/vendor/jquery.js"></script>
	<script src="js/foundation.min.js"></script>
	<script type="text/javascript">
		var COLORS = {
				warning : 'rgba(255,0,0,0.5)',
				normal : '#ffffff'
		};
		$(document).foundation();
		$(document).ready(function() {
			$('#submit').click(function(event) {
				if ($('#email').val() === '') {
					event.preventDefault();
					$('#email').css({'background-color' : COLORS.warning});
				}
			});
			if ( '${errorMessage}' && '${errorMessage}' != 'null') {
				window.alert('${errorMessage}');
				$('#message').css({'background-color' : COLORS.warning});
			}
			var keypressListener = function (event) {
				console.log(event);
				event.target.style['background-color'] = COLORS.normal;
				document.querySelector('#message').removeEventListener('keypress', keypressListener, false);
			}
			document.querySelector('#message').addEventListener('keypress', keypressListener, false);
		});
	</script>
</body>
</html>