<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Orders</title>
<style>
	div.row:nth-child(2n) {
		background-color: #ccc;
	}
</style>
</head>
<body>
	<c:forEach items="${orders}" var="order">
	<div class="row">
		<p class="small-only-text-center medium-6 columns"><b>Email:</b> ${order.email.address}</p>
		<p class="small-only-text-center medium-6 columns"><b>Product:</b> ${order.product.name}</p>
	</div>
	</c:forEach>
</body>
</html>