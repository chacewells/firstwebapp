<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Order for ${product.name}</title>
</head>
<body>
	<form method="POST" action="placeOrder">
	<div class="row">
		<div class="small-center small-10 small-offset-1 medium-12 columns">
			<div class="small-only-text-center medium-text-right medium-4 columns">
				<label for="email">Email: </label>
			</div>
			<div class="small-only-text-center medium-6 medium-pull-2 columns">
				<input type="text" name="email" id="email" />
			</div>
		</div>
		<div class="small-text-center small-center columns">
			<input class="button" type="submit" value="Submit" />
		</div>
	</div>
		<input type="hidden" name="product_id" value="${product.id}" />
	</form>
</body>
</html>