<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>My Really Awesome Products Page</title>
<style type="text/css">
	div.row:nth-child(2n + 1) {
		background-color: #ddd;
	}
	
	@media only screen and (max-width: 40em) {
		ul {
			list-style-type: none;
		}
	}
</style>
</head>
<body>
	<form id="products_form" method="POST" action="orderForm">
		<c:forEach items="${products}" var="product">
		<div class="row">
			<div class="small-center small-only-text-center medium-8 columns">
				<h3>${product.name}</h3>
				<ul>
					<li>${product.description}</li>
					<li><fmt:formatNumber type="currency">${product.price}</fmt:formatNumber></li>
				</ul>
			</div>
			<div class="small-text-center medium-4 small-text-right columns">
				<button value="${product.id}" type="button" class="order button">Order</button>
			</div>
		</div>
		</c:forEach>
		<input type="hidden" name="product_id" id="product_id" />
	</form>
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/vendor/jquery.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/foundation/foundation.js"></script>
	<script>
		$(document).ready(function () {
			$('.order').click(function (event) {
				var productId = $(this).attr('value');
				$('#product_id').attr('value', productId);
				$('#products_form').submit();
			});
		});
	</script>
</body>
</html>