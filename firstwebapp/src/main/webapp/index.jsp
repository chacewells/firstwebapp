<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@page import="java.io.PrintWriter"%>
<html>
<head><title>Hello World!</title></head>
<body>
<p>Time: <strong><%= new java.util.Date() %></strong></p>
<p>And here's the code:</p>
<%
String pageCode = "&lt;html&gt;\n" +
	"    &lt;body&gt;\n" +
	"        &lt;h2&gt;Hello World!&lt;/h2&gt;\n" +
	"        &lt;p&gt;Time: &lt;strong&gt;&lt;%= new java.util.Date() %&gt;&lt;/strong&gt;&lt;/p&gt;\n" +
	"    &lt;/body&gt;\n" +
	"&lt;/html&gt;";
%>
<textarea cols="65" rows="5">
<%= pageCode %>
</textarea>
</body>
</html>