package com.expeditors.training.web;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.expeditors.training.model.Email;
import com.expeditors.training.model.Product;
import com.expeditors.training.service.ProductService;

public class PlaceOrderServlet extends HttpServlet {
	
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		ProductService service = ProductService.getService();
		int productId = Integer.parseInt( request.getParameter("product_id") );
		String emailAddress = request.getParameter("email");
		service.addOrder(new Email(emailAddress), new Product(productId, "", "", 0.));
		
		response.sendRedirect(getServletContext().getContextPath() + "/orders");
	}
	
}
