package com.expeditors.training.web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class SubmitContactServlet extends HttpServlet {
	private static final String ERROR_TEMPLATE_RESOURCE = "firstwebapp/contact_servlet_form.html";
	
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		String email = request.getParameter("email");
		String message = request.getParameter("message");
		
		response.setContentType("text/html;charset=UTF-8");
		if ( message.isEmpty() ) {
			RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/WEB-INF/jsp/contact.jsp");
			request.setAttribute("errorMessage", "You must leave a message!");
			dispatcher.forward(request, response);
		} else {
			respondOkay(response, email);
		}
	}
	
	private void respondOkay(HttpServletResponse response, String email) throws IOException {
		PrintWriter out = response.getWriter();
		out.println("<!DOCTYPE html>");
		out.println("<html><head>");
		out.println("<title>Email Sent</title>");
		out.println("</head>");
		out.println("<body>");
		out.println("<p>We will respond within 24 hrs."
				+ " to your message at: " + email + "</p>");
		out.println("</body>");
		out.println("</html>");
	}
	
}
