package com.expeditors.training.web;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.expeditors.training.model.Product;
import com.expeditors.training.service.ProductService;

public class AddProductServlet extends HttpServlet {
	ProductService productService = ProductService.getService();
	
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		getServletContext().getRequestDispatcher("/WEB-INF/jsp/addProduct.jsp").forward(request, response);
	}
	
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String name = request.getParameter("name");
		String description = request.getParameter("description");
		double price = Double.parseDouble(request.getParameter("price"));
		
		productService.addProduct(new Product(name, description, price));
		
		response.sendRedirect(getServletContext().getContextPath() + "/products");
	}
	
}
