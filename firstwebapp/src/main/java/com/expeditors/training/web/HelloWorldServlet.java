package com.expeditors.training.web;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class HelloWorldServlet extends HttpServlet {
	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
		response.setContentType("text/html;charset=UTF-8");
		PrintWriter out = response.getWriter();
		
		out.println("<!DOCTYPE html>");
		out.println("<html><head>");
		out.println("<title>Hello, World</title>");
		out.println("</head>");
		out.println("<body>");
		out.println("<h2>Hello, World!</h2>");
		out.println("<p>Server Time: <strong>" + new Date() + "</strong></p>");
		out.println("</body></html>");
	}
}
