package com.expeditors.training.web;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.expeditors.training.model.Product;
import com.expeditors.training.service.ProductService;

public class OrderFormServlet extends HttpServlet {
	
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		ProductService service = ProductService.getService();
		long productId = Long.parseLong( request.getParameter("product_id") );
		Product product = service.getProductById(productId);
		request.setAttribute("product", product);
		
		getServletContext().getRequestDispatcher("/WEB-INF/jsp/orderForm.jsp").forward(request, response);
	}
	
}
