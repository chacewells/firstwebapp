package com.expeditors.training.model;

public class Order {
	long id;
	Email email;
	Product product;
	
	public Order(Email email, Product product) {
		this.email = email;
		this.product = product;
	}
	
	public Order(long id, Email email, Product product) {
		this.id = id;
		this.email = email;
		this.product = product;
	}
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Email getEmail() {
		return email;
	}
	public void setEmail(Email email) {
		this.email = email;
	}
	public Product getProduct() {
		return product;
	}
	public void setProduct(Product product) {
		this.product = product;
	}
}
