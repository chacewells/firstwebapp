package com.expeditors.training.model;

public class Email {
	long id;
	String address;
	
	public Email(long id, String address) {
		this.id = id;
		this.address = address;
	}
	public Email(String address) {
		this.address = address;
	}
	
	public long getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	
}
