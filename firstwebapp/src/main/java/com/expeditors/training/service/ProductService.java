package com.expeditors.training.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import com.expeditors.training.model.Email;
import com.expeditors.training.model.Order;
import com.expeditors.training.model.Product;

public class ProductService {
	static volatile ProductService productService;
	Connection conn;
	
	private ProductService(Properties properties) {
		try {
			Class.forName(properties.getProperty("JDBC_DRIVER"));
			conn = DriverManager.getConnection(
					properties.getProperty("DB_URL"),
					properties.getProperty("USER"),
					properties.getProperty("PASS"));
		} catch (SQLException|ClassNotFoundException e) {
			throw new RuntimeException(e);
		}
	}
	
	public static ProductService getService() {
		synchronized (ProductService.class) {
			if (productService == null) {
				synchronized(ProductService.class) {
					productService = new ProductService(getProperties());
				}
			}
		}
		
		return productService;
	}
	
	private static Properties getProperties() {
		Properties properties = new Properties();
		try (BufferedReader reader = 
				new BufferedReader(
						new InputStreamReader(
								ProductService.class
								.getClassLoader()
								.getResourceAsStream("/db.conf")));) {
		
			for (String line = reader.readLine(); line != null; line = reader.readLine()) {
				String[] props = line.split("=", 2);
				properties.put(props[0].trim(), props[1].trim());
			}
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return properties;
	}

	public List<Product> getProducts() {
		List<Product> products = new ArrayList<Product>();

		try {
			String sql = "SELECT product_id, name, description, price FROM product";
			for (
					ResultSet rs = conn.createStatement().executeQuery(sql); 
					rs.next();) {
				products.add(
						new Product(
								rs.getInt("product_id"),
								rs.getString("name"),
								rs.getString("description"), 
								rs.getDouble("price")));
			}

		} catch (SQLException e) {
			throw new RuntimeException(e);
		}

		return products;
	}
	
	public Product getProductById(long id) {
		Product product = null;
		String sql = "SELECT name, description, price FROM product WHERE product_id = ?";
		try {
			PreparedStatement stmt = conn.prepareStatement(sql);
			stmt.setLong(1, id);
			for (
					ResultSet rs = stmt.executeQuery();
					rs.next();) {
				product = new Product(id, rs.getString("name"), rs.getString("description"), rs.getDouble("price"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return product;
	}

	public synchronized void addProduct(Product product) {
		try {
			String sql = "INSERT INTO product (name, description, price) VALUES (?, ?, ?)";
			PreparedStatement stmt = conn.prepareStatement(sql);

			stmt.setString(1, product.getName());
			stmt.setString(2, product.getDescription());
			stmt.setDouble(3, product.getPrice());
			
			stmt.executeUpdate();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	public List<Email> getEmails() {
		List<Email> emails = new ArrayList<Email>();
		
		try {
			String sql = "SELECT email_id, email_address FROM email";
			for (
					ResultSet rs = conn.prepareStatement(sql).executeQuery();
					rs.next();
					emails.add(new Email(rs.getInt("email_id"), rs.getString("email_address"))));
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		
		return emails;
	}
	
	public Email getEmailById(long id) {
		Email email = null;
		
		try {
			String sql = "SELECT email_address FROM email WHERE email_id = ?";
			PreparedStatement stmt = conn.prepareStatement(sql);
			stmt.setLong(1, id);
			
			for (
					ResultSet rs = stmt.executeQuery(); 
					rs.next(); 
					email = new Email(id, rs.getString("email_address")));
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		
		return email;
	}
	
	public Email getEmailByAddress(String emailAddress) {
		Email email = null;
		String addressTrimmed = emailAddress.trim();
		
		try {
			String sql = "SELECT email_id FROM email WHERE email_address = ?";
			PreparedStatement stmt = conn.prepareStatement(sql);
			stmt.setString(1, addressTrimmed);
			
			for (
					ResultSet rs = stmt.executeQuery(); 
					rs.next(); 
					email = new Email(rs.getInt("email_id"), addressTrimmed ));
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		
		return email;
	}
	
	public synchronized void addEmail(Email email) {
		try {
			String sql = "INSERT INTO email(email_address) VALUES (?)";
			PreparedStatement stmt = conn.prepareStatement(sql);
			
			stmt.setString(1, email.getAddress());
			int success = stmt.executeUpdate();
			if (success > 0) {
				sql = "SELECT email_id FROM email WHERE email_address = ? FETCH FIRST 1 ROWS ONLY";
				stmt = conn.prepareStatement(sql);
				stmt.setString(1, email.getAddress());
				for (
						ResultSet rs = stmt.executeQuery();
						rs.next();
						email.setId(rs.getInt("email_id")));
			}
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	public List<Order> getOrders() {
		List<Order> orders = new ArrayList<Order>();
		
		try {
			String sql = "SELECT * FROM order_data";
			
			for (
					ResultSet rs = conn.createStatement().executeQuery(sql);
					rs.next();
					orders.add(
							new Order(
									rs.getInt("order_id"),
									new Email(
											rs.getLong("email_id"),
											rs.getString("email_address")),
									new Product(
											rs.getLong("product_id"),
											rs.getString("name"), 
											rs.getString("description"), 
											rs.getDouble("price")))));
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		
		return orders;
	}
	
	public synchronized void addOrder(Email email, Product product) {
		Email currentEmail = email;
		try {
			currentEmail = getEmailByAddress(email.getAddress().trim());
			
			if (currentEmail == null) {
				addEmail(email);
				currentEmail = email;
			}
			
			addOrder(new Order(currentEmail, product));
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	public synchronized void addOrder(Order order) {
		try {
			String sql = "INSERT INTO orders (email_id, product_id) VALUES (?, ?)";
			PreparedStatement stmt = conn.prepareStatement(sql);
			stmt.setLong(1, order.getEmail().getId());
			stmt.setLong(2, order.getProduct().getId());
			stmt.executeUpdate();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	
}
